package com.example.emked1.colorgame;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;


public class Settings {

    private static SharedPreferences sharedPreferences;

    public static void setUserTheme(Context context){

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean userTheme = sharedPreferences.getBoolean("darkTheme", false);

        if (userTheme){
            context.setTheme(R.style.Theme_AppCompat);
        }else{
            context.setTheme(R.style.Theme_AppCompat_DayNight);
        }
    }

    public static boolean isConnected(Context context){
        ConnectivityManager connMgr;
        NetworkInfo wifi;
        NetworkInfo mobile;
        boolean network;
        boolean isConnected;



        connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        network = sharedPreferences.getBoolean("network", false);

        isConnected = wifi != null && wifi.isConnectedOrConnecting() ||
                mobile != null && mobile.isConnectedOrConnecting();

        if (!network){
            isConnected = wifi != null && wifi.isConnectedOrConnecting();
        }

        return isConnected;
    }

    public static void setMode(){

        boolean hardMode = sharedPreferences.getBoolean("hardMode", false);

        if (hardMode){
            MainActivity.buttonNumbers = 9;
        }else{
            MainActivity.buttonNumbers = 6;
        }
    }
}
