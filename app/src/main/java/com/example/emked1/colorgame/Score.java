package com.example.emked1.colorgame;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Score {
    public String nickname;
    public int score;
    public String mode;

    public Score(){

    }

    public Score(String nickname, int score, String mode){
        this.nickname = nickname;
        this.score = score;
        this.mode = mode;
    }
}
