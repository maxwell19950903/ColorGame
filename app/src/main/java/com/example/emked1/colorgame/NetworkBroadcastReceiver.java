package com.example.emked1.colorgame;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.view.View;

import static com.example.emked1.colorgame.MainActivity.leaderBoardButton;

public class NetworkBroadcastReceiver extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sharedPreferences;
        ConnectivityManager connMgr;
        NetworkInfo wifi;
        NetworkInfo mobile;
        boolean network;
        boolean isConnected;

        connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        network = sharedPreferences.getBoolean("network", false);

        isConnected = wifi != null && wifi.isConnectedOrConnecting() ||
                mobile != null && mobile.isConnectedOrConnecting();

        if (!network){
            isConnected = wifi != null && wifi.isConnectedOrConnecting();
        }

        if (isConnected) {
            leaderBoardButton.setVisibility(View.VISIBLE);
        } else {
            leaderBoardButton.setVisibility(View.GONE);
        }

    }
}
