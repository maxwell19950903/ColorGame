package com.example.emked1.colorgame;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class ScoreAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Score> scores;


    public ScoreAdapter(Context context, ArrayList<Score> scores){
        this.context = context;
        this.scores = scores;
    }

    @Override
    public int getCount() {
        return scores.size();
    }

    @Override
    public Object getItem(int position) {
        return scores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        public TextView nickname;
        public TextView score;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.leaderboard_list_view, null);


            holder.nickname = convertView.findViewById(R.id.nicknameListItem);
            holder.score = convertView.findViewById(R.id.scoreListItem);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Collections.sort(scores, new Comparator<Score>() {
            @Override
            public int compare(Score score2, Score score1)
            {

                return  score1.score - score2.score;
            }
        });

        holder.nickname.setText(scores.get(position).nickname);
        holder.score.setText(String.valueOf(scores.get(position).score));

        return convertView;
    }
}
