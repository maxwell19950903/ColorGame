package com.example.emked1.colorgame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class LeaderboardActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DatabaseReference databaseReference;
        Query query;

        Settings.setUserTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);


        databaseReference = FirebaseDatabase.getInstance().getReference("users");
        query = databaseReference.orderByChild("score");
        query.addListenerForSingleValueEvent(valueEventListener);

    }

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            ListView listView;

            listView = findViewById(R.id.leaderboardListView);

            listView.setAdapter(new ScoreAdapter(getApplicationContext(), getLeaderBoard(dataSnapshot)));

        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e("Hiba", "onCancelled", databaseError.toException());
        }
    };

    private ArrayList<Score> getLeaderBoard(DataSnapshot dataSnapshot){
        ArrayList<Score> scores;

        scores = new ArrayList<>();

        for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
            Score user = singleSnapshot.getValue(Score.class);

            if (user != null) {
                scores.add(user);
            }
        }

        return scores;
    }


}
