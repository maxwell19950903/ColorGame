package com.example.emked1.colorgame;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {

    private Button startButton;
    private Button settingsButton;
    private EditText nickname;
    public static Button leaderBoardButton;
    private SharedPreferences sharedPreferences;
    public static int buttonNumbers;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Settings.setUserTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Settings.setMode();

        if (!checkPermission()){
            requestPermission();
        }

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);


        findViews();
        setListeners();


        nickname.setText(sharedPreferences.getString("nickname", ""));


        if (Settings.isConnected(this)) {
            leaderBoardButton.setVisibility(View.VISIBLE);
        } else {
            leaderBoardButton.setVisibility(View.GONE);
        }


    }



    View.OnClickListener action = new View.OnClickListener(){

        public void onClick(View v) {

            if(v == startButton){

                if (!(nickname.getText().toString().matches(""))){
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("nickname", nickname.getText().toString());
                    editor.apply();

                    Intent intentGame = new Intent(getApplicationContext() , GameActivity.class);
                    startActivity(intentGame);
                }else{
                    Toast.makeText(getApplicationContext(), "Kérem adjon megy egy nicknevet!", Toast.LENGTH_SHORT).show();
                }
            }else if (v == settingsButton){
                Intent intentSettings = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intentSettings);
            }else{
                Intent intentSettings = new Intent(getApplicationContext(), LeaderboardActivity.class);
                startActivity(intentSettings);
            }
        }
    };

    ValueEventListener scoreListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            int highscore = Integer.parseInt(sharedPreferences.getString("highscore", "0"));
            checkLeaderboard(dataSnapshot, highscore);

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.w("Hiba", "loadPost:onCancelled", databaseError.toException());
        }
    };


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (storageAccepted){

                    }
                    else {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                                showMessageOKCancel("A program működéséhez el kell fogadnod!",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("Rendben", okListener)
                .setCancelable(false)
                .create()
                .show();
    }

    private void findViews (){
        startButton = findViewById(R.id.startButton);
        settingsButton = findViewById(R.id.settingsButton);
        leaderBoardButton = findViewById(R.id.leaderBoardButton);
        nickname = findViewById(R.id.nicknameText);
    }

    private void setListeners (){
        startButton.setOnClickListener(action);
        settingsButton.setOnClickListener(action);
        leaderBoardButton.setOnClickListener(action);
        mFirebaseDatabase.addValueEventListener(scoreListener);
    }

    private void sendNotification(int highscore){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle("Új rekord:\t\t" + String.valueOf(highscore));
        mBuilder.setContentText("Döntsd meg most!");
        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    private void checkLeaderboard(DataSnapshot dataSnapshot, int highscore) {
        boolean changed = false;
        for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()){
            Score user = singleSnapshot.getValue(Score.class);

            if (user != null) {
                if (highscore < user.score){
                    highscore = user.score;
                    changed = true;

                }

            }
        }

        if (changed){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("highscore", String.valueOf(highscore));
            editor.apply();

            sendNotification(highscore);
        }
    }


}
