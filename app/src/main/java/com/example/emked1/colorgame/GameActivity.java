package com.example.emked1.colorgame;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

import static com.example.emked1.colorgame.MainActivity.buttonNumbers;

public class GameActivity extends AppCompatActivity {

    private Button colorButton1;
    private Button colorButton2;
    private Button colorButton3;
    private Button colorButton4;
    private Button colorButton5;
    private Button colorButton6;
    private Button colorButton7;
    private Button colorButton8;
    private Button colorButton9;

    private TextView colorText;
    private TextView timerText;

    private int correct;
    private int clickCounter = 0;

    private long startTime = 0L;
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;

    private Handler customHandler = new Handler();

    private String userID;

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private Random r = new Random();





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int[][] colors;
        String text;


        Settings.setUserTheme(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("users");

        findViews();
        setClickListeners();

        correct = r.nextInt(buttonNumbers);
        colors = generateColors();

        setButtonColors(colors);

        text = "RGB (" + colors[correct][0] + ", " + colors[correct][1] + ", " + colors[correct][2] + ")";
        colorText.setText(text);

        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
    }

    View.OnClickListener guess = new View.OnClickListener(){

        public void onClick(View v) {

            Button clicked = (Button) v;
            clickCounter++;
            if (clicked.getText().toString().equals(String.valueOf((correct + 1)))){

                int finalScore;
                String nickname;
                boolean isHardMode;
                String mode = "Könnyű";

                playSound(R.raw.won);

                timeSwapBuff += timeInMilliseconds;
                customHandler.removeCallbacks(updateTimerThread);

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                nickname = sharedPreferences.getString("nickname", "Nickname");
                isHardMode = sharedPreferences.getBoolean("hardMode", false);

                if (isHardMode){
                    mode = "Nehéz";
                }

                finalScore = (buttonNumbers - clickCounter + 1) * (30000 - Integer.parseInt(timerText.getText().toString().replace(":","")));

                createUser(nickname, finalScore, mode);

                createGameEndDialog(finalScore);

            }else{
                playSound(R.raw.buttonclicked);
                clicked.animate().alpha(0).rotationBy(360).scaleX(0).scaleY(0).setDuration(500).start();

            }

        }
    };

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            String text = "" + mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds);
            timerText.setText(text);
            customHandler.postDelayed(this, 0);
        }

    };

    private void createUser(String nickname, int score, String mode) {

        if (TextUtils.isEmpty(userID)) {
            userID = mFirebaseDatabase.push().getKey();
        }

        Score userScore = new Score(nickname, score, mode);

        mFirebaseDatabase.child(userID).setValue(userScore);

    }

    private void findViews (){
        colorButton1 = findViewById(R.id.colorButton1);
        colorButton2 = findViewById(R.id.colorButton2);
        colorButton3 = findViewById(R.id.colorButton3);
        colorButton4 = findViewById(R.id.colorButton4);
        colorButton5 = findViewById(R.id.colorButton5);
        colorButton6 = findViewById(R.id.colorButton6);
        colorButton7 = findViewById(R.id.colorButton7);
        colorButton8 = findViewById(R.id.colorButton8);
        colorButton9 = findViewById(R.id.colorButton9);

        colorText = findViewById(R.id.colorText);
        timerText = findViewById(R.id.timerText);
    }

    private void setClickListeners (){

        colorButton1.setOnClickListener(guess);
        colorButton2.setOnClickListener(guess);
        colorButton3.setOnClickListener(guess);
        colorButton4.setOnClickListener(guess);
        colorButton5.setOnClickListener(guess);
        colorButton6.setOnClickListener(guess);
        colorButton7.setOnClickListener(guess);
        colorButton8.setOnClickListener(guess);
        colorButton9.setOnClickListener(guess);

    }

    private int[][] generateColors(){

        int[][] colors = new int[buttonNumbers][3];

        for (int i = 0; i < buttonNumbers; i++){
            for (int j = 0; j < 3; j++) {
                colors[i][j] = r.nextInt(256);
            }
        }

        return colors;

    }

    private void setButtonColors(int[][] colors){

        colorButton1.setBackgroundColor(Color.argb(255,colors[0][0],colors[0][1],colors[0][2]));
        colorButton2.setBackgroundColor(Color.argb(255,colors[1][0],colors[1][1],colors[1][2]));
        colorButton3.setBackgroundColor(Color.argb(255,colors[2][0],colors[2][1],colors[2][2]));
        colorButton4.setBackgroundColor(Color.argb(255,colors[3][0],colors[3][1],colors[3][2]));
        colorButton5.setBackgroundColor(Color.argb(255,colors[4][0],colors[4][1],colors[4][2]));
        colorButton6.setBackgroundColor(Color.argb(255,colors[5][0],colors[5][1],colors[5][2]));

        colorButton1.setTextColor(Color.argb(255,colors[0][0],colors[0][1],colors[0][2]));
        colorButton2.setTextColor(Color.argb(255,colors[1][0],colors[1][1],colors[1][2]));
        colorButton6.setTextColor(Color.argb(255,colors[5][0],colors[5][1],colors[5][2]));
        colorButton5.setTextColor(Color.argb(255,colors[4][0],colors[4][1],colors[4][2]));
        colorButton4.setTextColor(Color.argb(255,colors[3][0],colors[3][1],colors[3][2]));
        colorButton3.setTextColor(Color.argb(255,colors[2][0],colors[2][1],colors[2][2]));

        if (buttonNumbers == 9) {
            colorButton7.setBackgroundColor(Color.argb(255, colors[6][0], colors[6][1], colors[6][2]));
            colorButton8.setBackgroundColor(Color.argb(255, colors[7][0], colors[7][1], colors[7][2]));
            colorButton9.setBackgroundColor(Color.argb(255, colors[8][0], colors[8][1], colors[8][2]));

            colorButton7.setTextColor(Color.argb(255, colors[6][0], colors[6][1], colors[6][2]));
            colorButton8.setTextColor(Color.argb(255, colors[7][0], colors[7][1], colors[7][2]));
            colorButton9.setTextColor(Color.argb(255, colors[8][0], colors[8][1], colors[8][2]));
        }else{
            colorButton7.setVisibility(View.GONE);
            colorButton8.setVisibility(View.GONE);
            colorButton9.setVisibility(View.GONE);
        }

    }

    private void createGameEndDialog(int finalScore){

        AlertDialog alertDialog = new AlertDialog.Builder(GameActivity.this).create();
        alertDialog.setMessage("A pontszámod: " + finalScore);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Új játék", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                finish();
                startActivity(getIntent());
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Beállítások", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                Intent intentSettings = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intentSettings);
            }
        });

        if (Settings.isConnected(this)){
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ranglista", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                    Intent intentSettings = new Intent(getApplicationContext(), LeaderboardActivity.class);
                    startActivity(intentSettings);
                }
            });
        }

        alertDialog.show();


    }

    public void playSound(int sound){
        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), sound);
        try {
            if (mp.isPlaying()) {
                mp.stop();
                mp.release();
                mp = MediaPlayer.create(getApplicationContext(), sound);
            } mp.start();
        } catch(Exception e) { e.printStackTrace(); }
    }
}
